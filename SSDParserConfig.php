<?php

/**
 * Configuration for parsing resources
 */
$resourceConfig = [

    /**
     * Configuration db
     */
    'DBConfig' => [
        'host' => 'localhost',
        'db_name' => 'ssdParser',
        'login' => 'root',
        'password' => 'root',
        'charset-set' => 'utf8'
       
    ],


    /**
     * Configuration for otzyvua.net
     */
    'otzyvua' => [
        'top' => [
            'links' => [
                ['http://www.otzyvua.net/', 100]
            ],
            'top_detail_product_links' => '.mheight a'
        ],
        'max_error' => 10,
        'detail_product_link' => '.x-large a.detailsLink',
        'phone_link' => '.link-phone',
        'pattern_phone_code' => "/\'id\':\'(\w+)[\']/",
        'links' => [
//            ['companies/it/', 150],
//            ['companies/aviakompanii/', 150],
//            ['companies/biznes/', 150],
            ['companies/bytovaya-tehnika/', 150],
//            ['companies/valyutniy-rynok/', 150],
//            ['companies/gruzoperevozki/', 150],
//            ['companies/gruzoperevozki-po-ukraine/', 150],
//            ['companies/dostavka-vody/', 150],
//            ['companies/dostavka-tsvetov/', 150],
//            ['companies/zavody/', 150],
//            ['companies/izdatelstvo-i-poligrafiya/', 150],
//            ['companies/investitsionnye-kompanii/', 150],
//            ['companies/internet-treyding/', 150],
//            ['companies/kantstovary/', 150],
//            ['companies/kliningovye-kompanii/', 150],
//            ['companies/kommunalnye-sluzhby/', 150],
//            ['companies/kosmetika-i-parfyumeriya/', 150],
//            ['companies/kurerskie-sluzhby/', 150],
//            ['companies/lizingovaya-kompaniya/', 150],
//            ['companies/marketingovye-agenstva/', 150],
//            ['companies/meditsina/', 150],
//            ['companies/mobilnye-telefony/', 150],
//            ['companies/molochnaya-produktsiya/', 150],
//            ['companies/companies/natyajnye-potolki/', 150],
//            ['companies/companies/nedvizhimost/', 150],
//            ['companies/operatory-mobilnoy-svyazi/', 150],
//            ['companies/organizatsiya-i-provedenie-prazdnikov/', 150],
//            ['companies/otzyvy-o-rabotodatelyah/', 150],
//            ['companies/otopitelnye-sistemy/', 150],
//            ['companies/ohrannye-kompanii/', 150],
//            ['companies/passajirskie-perevozki/', 150],
//            ['companies/pechat-vizitok/', 150],
//            ['companies/pishchevaya-promyshlennost/', 150],
//            ['companies/plastikovye-okna/', 150],
//            ['companies/podogrev-polov/', 150],
//            ['companies/polufabrikaty/', 150],
//            ['companies/pochta/', 150],
//            ['companies/poshiv-odezhdy/', 150],
//            ['companies/proizvoditeli-mebeli/', 150],
//            ['companies/proizvoditeli-napitkov/', 150],
//            ['companies/proizvodstvo/', 150],
//            ['companies/proizvodstvo-medikamentov/', 150],
//            ['companies/proizvodstvo-myasnoy-produktsii/', 150],
//            ['companies/proizvodstvo-okon/', 150],
//            ['companies/promyshlennoe-proizvodstvo/', 150],
//            ['companies/rabota-za-granitsey/', 150],
//            ['companies/reklamnye-uslugi/', 150],
//            ['companies/remont-kvartir/', 150],
//            ['companies/remont-noutbukov/', 150],
//            ['companies/remont-ofisnoy-tehniki/', 150],
//            ['companies/remont-tehniki/', 150],
//            ['companies/companies/santehnika/', 150],
//            ['companies/selskoe-hozyaystvo/', 150],
//            ['companies/sluzhba-dostavki/', 150],
//            ['companies/sluzhby-taksi/', 150],
//            ['companies/spravochnye-sluzhby/', 150],
//            ['companies/strahovye-kompanii/', 150],
//            ['companies/stroitelnaya-kompaniya/', 150],
//            ['companies/companies/telekommunikatsii-i-svyaz/', 150],
//            ['companies/teplye-poly/', 150],
//            ['companies/torgovo-montajnye-predpriyatiya/', 150],
//            ['companies/transportnye-uslugi/', 150],
//            ['companies/treyding-onlayn/', 150],
//            ['companies/turisticheskie-kompanii/', 150],
//            ['companies/fabriki/', 150],
//            ['companies/tsifrovoe-oborudovanie/', 150],
//            ['companies/chasy/', 150],
//            ['companies/shou-biznes/', 150],
//            ['companies/yuvelirnye-izdeliya/', 150],
//            ['companies/yuridicheskie-kompanii/', 150],
        ]
    ],






    /**
     * Sort of mobile phones
     */
    'mobile_sort' => [
        'mts' => '050|066|095|099|[\+]*[38]*050|[\+]*[38]*066|[\+]*[38]*095|[\+]*[38]*099',
        'lifecell' => '063|073|093|[\+]*[38]*063|[\+]*[38]*073|[\+]*[38]*093',
        'kyivstar' => '067|068|096|097|098|[\+]*[38]*067|[\+]*[38]*068|[\+]*[38]*096|[\+]*[38]*097|[\+]*[38]*098'
    ],

    /**
     * The path to the file with phone numbers
     */
    'patchToPhones' => './resource_phones',

    /**
     * Configuration for olx.ua
     */
    'olx' => [
        'top' => [
            'links' => [
                ['https://www.olx.ua/', 100]
            ],
//            'top_detail_product_links' => '.mheight a'
        ],
        'max_error' => 10,
        'detail_product_link' => '.x-large a.detailsLink',
        'phone_link' => '.link-phone',
        'pattern_phone_code' => "/\'id\':\'(\w+)[\']/",
        'links' => [
            ['moda-i-stil/odezhda/', 150],
            ['moda-i-stil/aksessuary/', 150],
            ['moda-i-stil/dlya-svadby/', 150],
            ['moda-i-stil/krasota-zdorove/', 150],
            ['elektronika/kompyutery-i-komplektuyuschie/', 150],
            ['nedvizhimost/arenda-kvartir/', 150],
            ['nedvizhimost/prodazha-kvartir/', 150],
            ['elektronika/noutbuki-i-aksesuary/', 150],
            ['elektronika/planshety-el-knigi-i-aksessuary/', 150],
            ['elektronika/tv-videotehnika/', 150],
            ['dom-i-sad/mebel/', 150],
            ['transport/legkovye-avtomobili/', 150],
            ['dom-i-sad/mebel/', 150],
            ['detskiy-mir/detskaya-odezhda/', 150]
        ]
    ],

    /**
     * Configuration for autoria
     */
    'autoria' => [
        'max_error' => 10,
        'phone_link' => '.js-phone',
        'links' => [
            ['uk/moto/', 100],
            ['uk/truck/', 100],
            ['uk/spectehnika/', 100],
            ['uk/legkovie/', 100],
            ['uk/bus/', 100],
            ['uk/trailer/', 100],
            ['uk/vodnyj-transport/', 100],
            ['uk/autohouse/', 13],
        ]
    ],

    /**
     * Configuration for bigl.ua
     */
    'bigl' => [
        'max_error' => 10,
        'data-attr-phone' => 'data-show-phones-phones',
        'links' => [
           ['Chehly-dlya-telefonov/', 200],
           ['Zaschitnaya-plenka-dlya-mobilnyh-telefonov/', 200],
           ['Naushniki-i-mikrofony/', 200],
           ['Mobilnye-telefony/', 200],
           ['Umnye-chasy-i-braslety/', 37],
           ['Radiotelefony/', 9],
           ['Tsifrovye-fotoaparaty/', 25]
        ]
    ],

    /**
     * Configuration for inforico
     */
    'inforico' => [
        'top' => [
            'links' => [
                ['http://inforico.com.ua/', 50]
            ],
            'top_detail_product_links' => '.ita-title'
        ],
        'max_error' => 10,
        'detail_product_link' => '.alvi-title',
        'phone_link' => '.avc-tels',
        'links' => [
            ['comp.inforico.com.ua/?_ga=2.91631071.432263486.1493968514-214394943.1493893496&page=', 100],
            ['tel.inforico.com.ua/?_ga=2.126214472.432263486.1493968514-214394943.1493893496&page=', 50],
            ['auto.inforico.com.ua/zapchasti-i-aksessuary-dlya-transportnoy-tehniki-c11/?_ga=2.37599845.431234653.1493968990-214394943.1493893496&page=', 50],
            ['odezhda.inforico.com.ua/odezhda-c124/?page=', 50],
            ['dom.inforico.com.ua/kvartiry-i-komnaty-c63/?_ga=2.127734346.615387631.1493969074-214394943.1493893496&page=', 50],
        ]
    ]

];
