<?php
/**
 * Code error in processing
 */

define('DB_CONNECT_ERROR', 'Error connecting to DB.');
define('UNCERTAIN_RESOURCE', 'Options for parsing wrong.');
define('NOT_PRODUCT_DETAILS_LINK', 'There was no reference to detailed product.');
define('NOT_PHONE_REQUEST_DATA', 'There were no attributes for phone.');
define('ERROR_REQUEST_PHONE_CURL', 'Can not get through the phone when prompted curl.');
define('ACCESS_DENIED', 'Access denied to resource.');
define('NOT_GET_PARAMETER', 'Not type parameter for parsing.');
define('NOT_DATA_FOR_PARSING', 'No corresponding parameter.');
define('NOT_DATA_ATTR', 'No attributes data for parse phone.');
define('NOT_FIND_PHONE', 'Not phones in element.');


/**
 * Connect library for parsing resources
 */
require_once 'library/simplehtmldom.php';

/**
 * Connect the main configuration file
 */
require_once './SSDParserConfig.php';

/**
 * Class for parsing phone numbers
 */
class SSDParser
{

    /**
     * Private class field
     */
    private $success;
    private $parseConfig;
    private $microtime = 0;
    private $DBConnect;
    private $activeObj;
    private $sim;
    private $processing_parse_error = '';

    /**
     * SSDParser constructor
     *
     * @param array $resourceConfig
     */
    public function __construct($resourceConfig)
    {
        try {
            /**
             * Starting microtime
             */
            $this->microtime = microtime(true);

            /**
             * Connect to DB
             */
            $this->DBConnect = new mysqli(
                $resourceConfig['DBConfig']['host'],
                $resourceConfig['DBConfig']['login'],
                $resourceConfig['DBConfig']['password'],
                $resourceConfig['DBConfig']['db_name']
            );
            if ($this->DBConnect->connect_error) throw new Exception(DB_CONNECT_ERROR);

            /**
             * Set parse config
             */
            $this->parseConfig['mobile_sort'] = $resourceConfig['mobile_sort'];
            $this->parseConfig['patchToPhones'] = $resourceConfig['patchToPhones'];
            $this->parseConfig['settings'] = $resourceConfig[$_GET['type']];

            /**
             * Initializes library simple_html_dom
             */
            $this->sim = new simple_html_dom();

            /**
             * Start parsing according to the type
             */
            switch ($_GET['type']) {
                case 'otzyvua' : {
                    $this->parseOtzyvua();
                };
                    break;
                case 'olx' : {
                    $this->parseOlx();
                };
                    break;
                case 'autoria' : {
                    $this->parseAutoria();
                };
                    break;
                case 'inforico' : {
                    $this->parseInforico();
                };
                    break;
                case 'save_file' : {
                    $this->saveFile();
                };
                    break;
                default: {
                    throw new Exception(NOT_GET_PARAMETER);
                }
            }

            if ($this->processing_parse_error) throw new Exception($this->processing_parse_error);

        } catch (Exception $e) {
            $this->success['error'] = $e->getMessage();
            $this->SetLog();
        }

        /**
         * Save in file
         */
        $this->saveFile();

        /**
         * Treat parsing result
         */
        $this->SetLog();
    }





    /**
     * @throws Exception
     */
    public function parseOtzyvua()
    {
        $this->activeObj = 'otzyvua';

        if (!isset($this->parseConfig['settings']['links']) || !count($this->parseConfig['settings']['links'])) throw new Exception(UNCERTAIN_RESOURCE);

        shuffle($this->parseConfig['settings']['links']);

        /**
         * To walk on all lines parsing
         */
        $companies = [];
        foreach ($this->parseConfig['settings']['links'] as $row) {

            $count_resource_error = 0;


            /**
             * Quit cycle when a certain number of errors in piercing link
             */
            if (isset($this->parseConfig['settings']['max_error']) && ($count_resource_error === $this->parseConfig['settings']['max_error'])) break;

            $url = 'http://www.otzyvua.net/' . $row[0];

            $result = $this->sim->load($this->getCUrl($url))->find('div.otzyv_box_float div.cnt_com a');
            if (count($result) > 0) {
                foreach ($result as $value) {
                    echo '<br>';
//                        $view_url=str_replace('.html','/',$value->attr['href']);
                    $query = $this->sim->load($this->getCUrl($value->attr['href'] . '/info'))->find('div#mainimg img');
                    $image = $query[0]->attr['src'];
                    $query = $this->sim->load($this->getCUrl($value->attr['href'] . '/info'))->find('span.value-title');
                    $date = $query[0]->attr['title'];
                    $name = $this->sim->load($this->getCUrl($value->attr['href'] . '/info'))->find('h1#itemname', 0)->plaintext;
                    $rate = $this->sim->load($this->getCUrl($value->attr['href'] . '/info'))->find('div#unit_long strong', 0)->plaintext;
                    $description = trim($this->sim->load($this->getCUrl($value->attr['href'] . '/info'))->find('div[class=pads clearfix description1]', 0)->plaintext);
//                    $name= mb_convert_encoding($name,'UTF-8');
//                    $description= mb_convert_encoding($description,'UTF-8');
                    $companies[$name] = ['name' => $name, 'date' => $date, 'rate' => $rate, 'description' => $description, 'image' => $image];
                    $href_comments = [];
                    for ($i = 1; $i > 0; $i++) {
                        $a_comments = $this->sim->load($this->getCUrl($value->attr['href'] . '?page=' . $i))->find('div.comment_row h2 a');
                        if ($a_comments) {
                            foreach ($a_comments as $a) {
                                $href_comments[] = $a->attr['href'];
                            }
                        } else {
                            break;
                        }
                    }
                    foreach ($href_comments as $href_comment) {
                        $comment_text = trim($this->sim->load($this->getCUrl($href_comment))->find('div[class=commentbox review] span[class=comment description]', 0)->plaintext);
                        $author = trim($this->sim->load($this->getCUrl($href_comment))->find('div[class=commentbox review] span[class=author_name] ins', 0)->plaintext);
                        $date = trim(str_replace($author, '', $this->sim->load($this->getCUrl($href_comment))->find('div[class=commentbox review] span[class=author_name]', 0)->innertext));
                        $title= trim(str_replace('- Отзыв о '.$name, '', $this->sim->load($this->getCUrl($href_comment))->find('h1[class=element_name]', 0)->innertext));
                        $comment_text= mb_convert_encoding($comment_text,"Windows-1252","UTF-8");
                        $author = html_entity_decode($author);
                        $title= html_entity_decode($title);
//                        $date= mb_convert_encoding($date,'UTF-8');
//                        $date= mb_convert_encoding($date,'UTF-8');
                        $companies[$name]['comments'][] = ['text' => $comment_text, 'date' => $date, 'author' => $author,'title'=>$title];
                    }
                    file_put_contents(dirname(__FILE__).'/companies/'.time().'.json',json_encode($companies,JSON_UNESCAPED_UNICODE));
                    $result=$this->DBConnect->query("INSERT INTO companies (description, date_create, image, rate,desc_mini,`name` ) VALUES ('"
                        .$companies[$name]['description'] ."','".$companies[$name]['date']."','".$companies[$name]['image']."','".$companies[$name]['rate']."','".substr($companies[$name]['description'],120)."...','".$companies[$name]['name']."')");
                    $company_id=$this->DBConnect->query("SELECT id FROM companies WHERE name='".$name."'")->fetch_assoc();
                    foreach ($companies[$name]['comments'] as $comment)
                    {
                        $this->DBConnect->query("INSERT INTO coments (description, date_create, author, title, company_id) VALUES ('"
                            .$comment['text'] ."','".$comment['date']."','".$comment['author']."','".$comment['title']."','".$company_id['id']."')");
                    }
                    echo '=';die;
                }
            }
        }
    }


    /**
     * Parsing site auto.ria.com
     */
    public function parseAutoria()
    {

        $this->activeObj = 'autoria';

        if (!isset($this->parseConfig['settings']['links']) || !count($this->parseConfig['settings']['links'])) throw new Exception(UNCERTAIN_RESOURCE);

        shuffle($this->parseConfig['settings']['links']);

        /**
         * To walk on all lines parsing
         */

        foreach ($this->parseConfig['settings']['links'] as $row) {

            $count_resource_error = 0;

            $i = 1;
            do {

                /**
                 * Quit cycle when a certain number of errors in piercing link
                 */
                if (isset($this->parseConfig['settings']['max_error']) && ($count_resource_error === $this->parseConfig['settings']['max_error'])) break;

                $url = 'https://auto.ria.com/' . $row[0] . (($i > 1 && $row[1]) ? '?page=' . $i : '');

                $result = $this->sim->load($this->getCUrl($url))->find($this->parseConfig['settings']['phone_link']);

                if (count($result) > 0) {
                    foreach ($result as $value) {
                        if (preg_match('/(\+?[\d-\(\)\s]{8,20}[0-9]?\d)/', $value->innertext)) $this->insertPhone(str_replace(array(' ', '(', ')', '-'), array('', '', '', ''), $value->innertext));
                    }
                } else {
                    $this->processing_parse_error .= $url . ' - ' . NOT_DATA_FOR_PARSING . "\n";
                    $count_resource_error++;
                }
                $i++;
            } while ($i <= $row[1] ? true : false);
        }
    }

    /**
     * Parsing site olx.ua
     */
    public function parseOlx()
    {

        $this->activeObj = 'olx.ua';

        /**
         * Top parse
         */
        if (isset($this->parseConfig['settings']['top']['links']) and count($this->parseConfig['settings']['top']['links']) > 0) {

            shuffle($this->parseConfig['settings']['top']['links']);

            foreach ($this->parseConfig['settings']['top']['links'] as $row) {

                $count_resource_error = 0;

                $i = 1;
                do {
                    if (isset($this->parseConfig['settings']['max_error']) && ($count_resource_error === $this->parseConfig['settings']['max_error'])) break;
                    $detail_links = file_get_html($row[0])->find($this->parseConfig['settings']['top']['top_detail_product_links']);
                    if (count($detail_links) > 0) {
                        foreach ($detail_links as $value) {

                            $c_content = $this->getCUrl($value->href, '', true);

                            /**
                             * Get session php id from resource
                             */
                            preg_match('/^Set-Cookie:\s*([^;]*)/mi', $c_content, $m);
                            parse_str($m[1], $cookies);

                            /**
                             * Get the token phone from request
                             */
                            preg_match('/phoneToken[ =\']+(.*)\'/', $c_content, $tokenPhone);

                            $get_phone_attr = $this->sim->load($c_content)->find($this->parseConfig['settings']['phone_link'], 0)->attr;
                            if (isset($get_phone_attr['class'])) {
                                preg_match($this->parseConfig['settings']['pattern_phone_code'], $get_phone_attr['class'], $setPhone);
                                if (isset($setPhone[1])) {
                                    $phones = $this->getCUrl('https://www.olx.ua/ajax/misc/contact/phone/' . $setPhone[1] . '/?pt=' . $tokenPhone[1], 'PHPSESSID=' . $cookies['PHPSESSID'] . ';' . 'path=/');
                                    if ($phones) {
                                        if (!preg_match('/Access Denied/i', $phones)) {
                                            preg_replace_callback(
                                                '/(\+?[\d-\(\)\s]{8,20}[0-9]?\d)/',
                                                function ($matches) {
                                                    if (isset($matches[0])) $this->insertPhone(str_replace(array(' ', '(', ')', '-'), array('', '', '', ''), $matches[0]));
                                                },
                                                $phones
                                            );
                                        } else throw new Exception(ACCESS_DENIED);
                                    } else $this->processing_parse_error .= 'Top - ' . $value->href . ' - ' . ERROR_REQUEST_PHONE_CURL . "\n";
                                }
                            } else $this->processing_parse_error .= 'Top - ' . $value->href . ' - ' . NOT_PHONE_REQUEST_DATA . "\n";
                        }
                    } else {
                        $this->processing_parse_error .= 'Top - ' . $row[0] . ' - ' . NOT_PRODUCT_DETAILS_LINK . "\n";
                        $count_resource_error++;
                    }

                    $i++;
                } while ($i <= $row[1]);

            }

        }

        /**
         * Standart parse
         */
        if (!isset($this->parseConfig['settings']['links']) || !count($this->parseConfig['settings']['links'])) throw new Exception(UNCERTAIN_RESOURCE);
        shuffle($this->parseConfig['settings']['links']);

        /**
         * To walk on all lines parsing
         */
        foreach ($this->parseConfig['settings']['links'] as $row) {

            $count_resource_error = 0;

            $i = 1;
            do {
                /**
                 * Quit cycle when a certain number of errors in piercing link
                 */
                if (isset($this->parseConfig['settings']['max_error']) && ($count_resource_error === $this->parseConfig['settings']['max_error'])) break;

                $url = 'https://olx.ua/' . $row[0] . (($i > 1 && $row[1]) ? '?page=' . $i : '');
                $html_page_content = file_get_html($url)->find($this->parseConfig['settings']['detail_product_link']);

                if (count($html_page_content) > 0) {
                    foreach ($html_page_content as $value) {

                        $c_content = $this->getCUrl($value->href, '', true);

                        /**
                         * Get session php id from resource
                         */
                        preg_match('/^Set-Cookie:\s*([^;]*)/mi', $c_content, $m);
                        parse_str($m[1], $cookies);

                        /**
                         * Get the token phone from request
                         */
                        preg_match('/phoneToken[ =\']+(.*)\'/', $c_content, $tokenPhone);

                        $get_phone_attr = $this->sim->load($c_content)->find($this->parseConfig['settings']['phone_link'], 0)->attr;
                        if (isset($get_phone_attr['class'])) {
                            preg_match($this->parseConfig['settings']['pattern_phone_code'], $get_phone_attr['class'], $set_phone);
                            if (isset($set_phone[1])) {
                                $phones = $this->getCUrl('https://www.olx.ua/ajax/misc/contact/phone/' . $set_phone[1] . '/?pt=' . $tokenPhone[1], 'PHPSESSID=' . $cookies['PHPSESSID'] . ';' . 'path=/');
                                if ($phones) {
                                    if (!preg_match('/Access Denied/i', $phones)) {
                                        preg_replace_callback(
                                            '/(\+?[\d-\(\)\s]{8,20}[0-9]?\d)/',
                                            function ($matches) {
                                                if (isset($matches[0])) $this->insertPhone(str_replace(array(' ', '(', ')', '-'), array('', '', '', ''), $matches[0]));
                                            },
                                            $phones
                                        );
                                    } else throw new Exception(ACCESS_DENIED);
                                } else $this->processing_parse_error .= $value->href . ' - ' . ERROR_REQUEST_PHONE_CURL . "\n";
                            }
                        } else $this->processing_parse_error .= $value->href . ' - ' . NOT_PHONE_REQUEST_DATA . "\n";
                    }
                } else {
                    $this->processing_parse_error .= $url . ' - ' . NOT_PRODUCT_DETAILS_LINK . "\n";
                    $count_resource_error++;
                }
                $i++;
            } while ($i <= $row[1] ? true : false);
        }
    }

    /**
     * Parsing site Inforico
     */
    public function parseInforico()
    {

        $this->activeObj = 'inforico';

        if (isset($this->parseConfig['settings']['top']['links']) and count($this->parseConfig['settings']['top']['links']) > 0) {

            shuffle($this->parseConfig['settings']['top']['links']);
            foreach ($this->parseConfig['settings']['top']['links'] as $row) {

                $count_resource_error = 0;

                $i = 1;
                do {

                    if (isset($this->parseConfig['settings']['max_error']) && ($count_resource_error === $this->parseConfig['settings']['max_error'])) break;

                    $details_link = file_get_html($row[0])->find($this->parseConfig['settings']['top']['top_detail_product_links'] . ' a');
                    if (count($details_link) > 0) {
                        foreach ($details_link as $value) {
                            $phones = file_get_html($value->href)->find($this->parseConfig['settings']['phone_link'])[0]->innertext;
                            if ($phones) {
                                preg_replace_callback(
                                    '/(\+?[\d-\(\) ]{8,20})/',
                                    function ($matches) {
                                        if (isset($matches[0])) $this->insertPhone(str_replace(array(' ', '(', ')', '-'), array('', '', '', ''), $matches[0]));
                                    },
                                    $phones
                                );
                            } else $this->processing_parse_error .= $value->href . ' - ' . NOT_FIND_PHONE . "\n";
                        }
                    } else {
                        $this->processing_parse_error .= 'Top - ' . $row[0] . ' - ' . NOT_PRODUCT_DETAILS_LINK . "\n";
                        $count_resource_error++;
                    }
                    $i++;
                } while ($i <= $row[1]);
            }
        }

        if (!isset($this->parseConfig['settings']['links']) || !count($this->parseConfig['settings']['links'])) throw new Exception(UNCERTAIN_RESOURCE);
        shuffle($this->parseConfig['settings']['links']);

        /**
         * To walk on all lines parsing
         */
        foreach ($this->parseConfig['settings']['links'] as $row) {

            $count_resource_error = 0;

            $i = 1;
            do {

                /**
                 * Quit cycle when a certain number of errors in piercing link
                 */
                if (isset($this->parseConfig['settings']['max_error']) && ($count_resource_error === $this->parseConfig['settings']['max_error'])) break;

                $url = 'http://' . $row[0] . (($i > 1 && $row[1]) ? $i : 1);

                $html_page_content = file_get_html($url)->find($this->parseConfig['settings']['detail_product_link'] . ' a');
                if (count($html_page_content) > 0) {
                    foreach ($html_page_content as $value) {
                        $phones = file_get_html($value->href)->find($this->parseConfig['settings']['phone_link'])[0]->innertext;
                        if ($phones) {
                            preg_replace_callback(
                                '/(\+?[\d-\(\) ]{8,20})/',
                                function ($matches) {
                                    if (isset($matches[0])) $this->insertPhone(str_replace(array(' ', '(', ')', '-'), array('', '', '', ''), $matches[0]));
                                },
                                $phones
                            );
                        } else $this->processing_parse_error .= $value->href . ' - ' . NOT_FIND_PHONE . "\n";
                    }
                } else {
                    $this->processing_parse_error .= $url . ' - ' . NOT_PRODUCT_DETAILS_LINK . "\n";
                    $count_resource_error++;
                }
                $i++;
            } while ($i <= $row[1] ? true : false);
        }


    }

    /**
     * Check for the existence url address
     *
     * @param string $url
     * @return bool
     */
    public function IsUrl($url)
    {
        $headers = get_headers($url, TRUE);
        if (isset($headers[2]) and preg_match('/404/', $headers[2])) return false;
        else return true;
    }

    /**
     * Curl init
     *
     * @param $url
     * @param $c_cookie
     * @param $header
     *
     * @return string
     */
    public function getCUrl($url, $c_cookie = '', $header = false)
    {
        $curl = curl_init();
        if ($curl) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HEADER, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, $header);
            if ($c_cookie) {
                curl_setopt($curl, CURLOPT_COOKIE, $c_cookie);
            }
            curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36');
            $out = curl_exec($curl);
            curl_close($curl);
            return $out;
        } else return false;
    }

    /**
     * Insert a new number in the database
     *
     * @param string $phone
     */
    public function insertPhone($phone)
    {
        $this->DBConnect->query("INSERT INTO `" . $this->activeObj . "` (phone) VALUES ('" . $phone . "')");
    }

    /**
     * Insert a new number in the database
     *
     * @return array
     */
    public function getPhone()
    {
        return $this->DBConnect->query("(SELECT phone FROM autoria ORDER BY id ASC) UNION (SELECT phone FROM `olx.ua` ORDER BY id ASC) UNION (SELECT phone FROM `inforico` ORDER BY id ASC)");
    }

    /**
     * Save phone in file
     *
     */
    public function saveFile()
    {
        $phones = $this->getPhone();
        if ($phones->num_rows > 0) {
            $path = $this->parseConfig['patchToPhones'] . '/';

            if (!is_dir($path)) mkdir($path);
            else {
                if (file_exists($path)) foreach (glob($path . '/*') as $file) unlink($file);
            }

            $part = array();

            while ($row = $phones->fetch_assoc()) {
                $file_name = false;
                $phone = $row['phone'];
                foreach ($this->parseConfig['mobile_sort'] as $key => $item) {
                    if (preg_match('/^(' . $item . ')/', $phone)) {
                        $file_name = $key;
                        break;
                    };
                }

                if (!$file_name) $file_name = 'other';

                if (isset($part[$file_name])) $file_path = $path . 'part_' . $part[$file_name] . '_' . $file_name;
                else $file_path = $path . $file_name;

                if (file_exists($file_path . '.txt') AND (filesize($file_path . '.txt') > 512000)) {
                    if (isset($part[$file_name])) $part[$file_name] += 1; else $part[$file_name] = 1;
                }

                file_put_contents($file_path . '.txt', $phone . "\n", FILE_APPEND);
            }
        }
    }

    /**
     * The method opens the file and asks when the
     * last time stamp was made and the result of parsing parsing
     */
    public function SetLog()
    {

        $template = array(
            'header' => "------------------------------------\nResult parsing" . ($this->activeObj ? ' - ' . $this->activeObj : '.') . ' Date: ' . date('d.m.Y H:i:s', time()) . "\n",
            'description' => ''
        );

        if (isset($this->success['error'])) {
            $template['description'] = "When parsing pass following error:\n";
            $template['description'] .= $this->success['error'] . "\n";
        } else $template['description'] .= "When parsing errors were found.\n";
        $template['description'] .= 'Total time parser: ' . ceil(microtime(true) - $this->microtime) . ' seconds.' . "\n";

        file_put_contents('./SSDParserLog.txt', stripslashes(implode('', $template)), FILE_APPEND);
        exit();
    }
}

/**
 * Initialization object class
 */
new SSDParser($resourceConfig);

