<?php

/**
 * Class SSDParserApi
 */

/**
 * Code error in processing
 */
define('NOT_REGISTER_METHOD', 'Unregistered method or empty method.');
define('NOT_DIRECTORY_FILES', 'Directory files not ready.');
define('NOT_POST_FIELD', 'No data transmitted faithful post.');
define('NOT_FILE', 'File not found.');

class SSDParserApi
{

    private $uri = '';
    private $callMethod = [
        'getDirectoryFiles',
        'getFile'
    ];
    private $result = array(
        'success' => FALSE
    );
    private $path = './resource_phones';

    public function __construct()
    {
        $this->uri = $_SERVER['REQUEST_URI'];

        /**
         * Call functions under the url string
         */
        $this->callMethod();
    }

    /**
     * Distributor
     */
    public function callMethod() {
        try {
            $method = array_pop(explode('/', $this->uri));
            if(in_array($method, $this->callMethod)) $this->$method();
             else throw new Exception(NOT_REGISTER_METHOD);
        } catch (Exception $e) {
            $this->result['error'] = $e->getMessage();
        }
    }

    /**
     * Get directory files
     */
    public function getDirectoryFiles() {
        try {
            if(is_dir($this->path)) {
                $_files = [];
                $files = scandir($this->path);
                foreach ($files as $key => $value) {
                    if (!in_array($value, array('.', '..')))
                        $_files[] = $value;
                }
                if(!count($_files)) throw new Exception(NOT_DIRECTORY_FILES);
            } else throw new Exception(NOT_DIRECTORY_FILES);
            $this->result['success'] = TRUE;
            $this->result['type'] = 'DirectoryFiles';
            $this->result['response'] = $_files;
        } catch (Exception $e) {
            $this->result['error'] = $e->getMessage();
        }
    }

    /**
     * Get file
     */
    public function getFile() {
        try {
            $file_name = $_POST['file_name'];
            if(!$file_name) throw new Exception(NOT_POST_FIELD);
            $path = $this->path . '/' . $file_name;
            if(!file_exists($path)) throw new Exception(NOT_FILE);
            $this->result['success'] = TRUE;
            $this->result['type'] = 'fileData';
            $this->result['response'] = file($path, FILE_IGNORE_NEW_LINES);
        }
        catch (Exception $e) {
            $this->result['error'] = $e->getMessage();
        }
    }

    /**
     * Return result
     */
    public function getResult() {
        echo json_encode($this->result);
    }
}

$SSDParserApi = new SSDParserApi();
$SSDParserApi->getResult();