<?php

/**
 * Configuration for parsing resources
 */
$resourceConfig = [

    /**
     * Configuration db
     */
    'DBConfig' => [
        'host' => 'localhost',
        'db_name' => 'ssdParser',
        'login' => 'andrey',
        'password' => 'programeriua'
    ],

    /**
     * Sort of mobile phones
     */
    'mobile_sort' => [
        'MTS_Vodafone' => '050|066|095|099',
        'Lifecell' => '063|073|093',
        'Kyivstar' => '067|068|096|097|098'
    ],

    /**
     * The path to the file with phone numbers
     */
    'patchToPhones' => './resource_phones',

    /**
     * Configuration for olx.ua
     */
    'olx' => [
        'top' => [
            'links' => [
                ['https://www.olx.ua/', 100]
            ],
            'top_detail_product_links' => '.mheight a'
        ],
        'max_error' => 10,
        'detail_product_link' => '.x-large a.detailsLink',
        'phone_link' => '.link-phone',
        'pattern_phone_code' => "/\'id\':\'(\w+)[\']/",
        'links' => [
            ['moda-i-stil/odezhda/', 150],
            ['moda-i-stil/aksessuary/', 150],
            ['moda-i-stil/dlya-svadby/', 150],
            ['moda-i-stil/krasota-zdorove/', 150],
            ['elektronika/kompyutery-i-komplektuyuschie/', 150],
            ['nedvizhimost/arenda-kvartir/', 150],
            ['nedvizhimost/prodazha-kvartir/', 150],
            ['elektronika/noutbuki-i-aksesuary/', 150],
            ['elektronika/planshety-el-knigi-i-aksessuary/', 150],
            ['elektronika/tv-videotehnika/', 150],
            ['dom-i-sad/mebel/', 150],
            ['transport/legkovye-avtomobili/', 150],
            ['dom-i-sad/mebel/', 150],
            ['detskiy-mir/detskaya-odezhda/', 150]
        ]
    ],

    /**
     * Configuration for autoria
     */
    'autoria' => [
        'max_error' => 10,
        'phone_link' => '.js-phone',
        'links' => [
            ['uk/moto/', 100],
            ['uk/truck/', 100],
            ['uk/spectehnika/', 100],
            ['uk/legkovie/', 100],
            ['uk/bus/', 100],
            ['uk/trailer/', 100],
            ['uk/vodnyj-transport/', 100],
            ['uk/autohouse/', 13],
        ]
    ],

    /**
     * Configuration for bigl.ua
     */
    'bigl' => [
        'max_error' => 10,
        'data-attr-phone' => 'data-show-phones-phones',
        'links' => [
           ['Chehly-dlya-telefonov/', 200],
           ['Zaschitnaya-plenka-dlya-mobilnyh-telefonov/', 200],
           ['Naushniki-i-mikrofony/', 200],
           ['Mobilnye-telefony/', 200],
           ['Umnye-chasy-i-braslety/', 37],
           ['Radiotelefony/', 9],
           ['Tsifrovye-fotoaparaty/', 25]
        ]
    ],

    /**
     * Configuration for inforico
     */
    'inforico' => [
        'top' => [
            'links' => [
                ['http://inforico.com.ua/', 50]
            ],
            'top_detail_product_links' => '.ita-title'
        ],
        'max_error' => 10,
        'detail_product_link' => '.alvi-title',
        'phone_link' => '.avc-tels',
        'links' => [
            ['comp.inforico.com.ua/?_ga=2.91631071.432263486.1493968514-214394943.1493893496&page=', 100],
            ['tel.inforico.com.ua/?_ga=2.126214472.432263486.1493968514-214394943.1493893496&page=', 50],
            ['auto.inforico.com.ua/zapchasti-i-aksessuary-dlya-transportnoy-tehniki-c11/?_ga=2.37599845.431234653.1493968990-214394943.1493893496&page=', 50],
            ['odezhda.inforico.com.ua/odezhda-c124/?page=', 50],
            ['dom.inforico.com.ua/kvartiry-i-komnaty-c63/?_ga=2.127734346.615387631.1493969074-214394943.1493893496&page=', 50],
        ]
    ]

];
